package com.example.mascotavirtual;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

//Representación gráfica de item infinito
public class Infinito extends Button {
    Infinito(Juguete juguete, Inventario inventario, Mascota mascota) throws FileNotFoundException {
        super();
        Image imagen = new Image(new FileInputStream(juguete.get_path_img()));
        ImageView img_view = new ImageView(imagen);
        img_view.setPreserveRatio(true);
        img_view.setFitHeight(90);
        img_view.setFitWidth(90);
        this.setOnAction(e -> mascota.usar_item(inventario.sacar_item(juguete.get_id())));
        this.setGraphic(img_view);
    }
}
