package com.example.mascotavirtual;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.PerspectiveCamera;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;

// Clase encargada de la conexión entre Interfaz Gráfica y Mascota
public class Controller {
    private Mascota mascota;
    private Inventario inventario;
    private BooleanProperty pausado;
    private Timeline timeline;

    public Controller(){
        mascota = new Mascota("Garfield");
        inventario = new Inventario();
        pausado = new SimpleBooleanProperty(true);


        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(true);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                e -> mascota.timeStep(),
                (KeyValue) null));

        inventario.agregar_items(new Comida(1, "Leche",3 ));
        inventario.agregar_items(new Comida(2, "Arroz",4 ));
        inventario.agregar_items(new Comida(3, "Carne",5 ));

        inventario.agregar_items(new Medicina(4, "Jarabe",4));
        inventario.agregar_items(new Medicina(5, "Pastilla",4 ));

        inventario.agregar_items(new Juguete(6, "Pelota", "images/toys/ball.png"));
        inventario.agregar_items(new Juguete(7, "Hueso", "images/toys/bone.png"));
    }

    //Agrega items del inventario a los GridPanes
    public void rellenar_grids(GridPane alimentos, GridPane medicina, GridPane juguete) throws FileNotFoundException {
        inventario.rellenar_grids(alimentos, medicina, juguete, mascota);
    }

    //Configura los botones del menú de inicio
    public void bind_menu_inicio(MenuItem iniciar, MenuItem reiniciar, MenuItem salir, Stage stage){
        iniciar.setOnAction(e -> timeline.play());
        iniciar.visibleProperty().bind(pausado);
        salir.setOnAction(e -> stage.close());
        //funcionalidad de reiniciar en la siguiente etapa
    }

    //Configura los botones del menú de acciones,
    public void bind_menu_acciones(MenuItem apagar, MenuItem prender, ImageView fondo_dia, ImageView fondo_noche){
        apagar.setOnAction(e -> {
            mascota.dormir();
            fondo_dia.visibleProperty().set(false);
            fondo_noche.visibleProperty().set(true);
        });
        apagar.visibleProperty().bind(mascota.isDormida().not());

        prender.setOnAction(e -> {
            mascota.despertar();
            fondo_dia.visibleProperty().set(true);
            fondo_noche.visibleProperty().set(false);
        });
        prender.visibleProperty().bind(mascota.isDormida());
    }

    //Bind a las propiedades de la mascota con las barras
    public void bind_barras(ProgressBar salud, ProgressBar energia, ProgressBar felicidad){
        salud.progressProperty().bind(mascota.getSalud().divide(100));
        energia.progressProperty().bind(mascota.getEnergia().divide(100));
        felicidad.progressProperty().bind(mascota.getFelicidad().divide(100));
    }

    //Bind a propiedades de texto de la mascota con los labels
    public void bind_info(Label nombre, Label edad, Label estado){
        nombre.textProperty().bind(mascota.getNombre());
        edad.textProperty().bind(mascota.getEdad().asString());
        estado.textProperty().bind(mascota.getEstado());
    }
}
