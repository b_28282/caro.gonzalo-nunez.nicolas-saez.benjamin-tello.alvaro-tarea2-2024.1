package com.example.mascotavirtual;

public class Comida extends Item{
    
    public Comida(Integer id, String nombre, Integer cantidad){
        super(id, nombre, cantidad);
        this.energia = 20;
        this.salud = 20;
    }
}
