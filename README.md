DISEÑO Y PROGRAMACIÓN ORIENTADA A OBJETOS (ELO - 329)
DEPARTAMENTO DE ELECTRONICA
UNIVERSIDAD TÉCNICA FEDERICO SANTA MARÍA

# TAREA Nº2: Simulación de una mascota virtual en JavaFX

Estudiantes:        
- Gonzalo Caro    ROL: 202030553 - 6
- Nicolas Núñez   ROL: 202130502 - 5
- Benjamín Saez   ROL: 202173616 - 6
- Álvaro Tello    ROL: 201930552 - 2

# I. Concepto de Tarea y Funcionamiento

La tarea consiste en la simulación del comportamiento de una mascota virtual, ahora con una interfaz gráfica. 
Se optó por la bonificación extra, que consiste en agregarle una animación horizontal a la mascota. 
El programa lee desde un archivo config.csv para obtener el nombre de la mascota y el estado inicial del inventario.  
Al iniciar el programa, se mostrará la vista principal del juego. Para comenzar o reiniciar el juego, se
elige la opción correspondiente en el menú superior de inicio.  
Cuando comienza el juego, este seguirá funcionando hasta que la mascota muera. Se debe cuidar de la mascota  
con los items del inventario, y permitiéndole dormir y despertar en el menú de Acciones.  

Si se quieren cambiar los atributos del archivo config, este se encuentra en Etapa X\config\config.csv  
Además, las imagenes se encuentran en Etapa X\images  
El código fuente se encuentra en Etapa X\src\main\java\com\example, y sus archivos son:  
-App.java  
-Comida.java  
-Consumible.java  
-Controller.java  
-Estado.java  
-Infinito.java  
-Inventario.java  
-Item.java  
-Juguete.java  
-Mascota.java  
-Medicina.java  


# II. Ejecución del Programa
Se entrega un proyecto de IntelliJ en cada etapa.  
Para ejecutarlo, se debe abrir el proyecto desde la carpeta correspondiente a la etapa.  
Luego, se debe ejecutar App.java, encontrado en Etapa X\src\main\java\com\example\App.java  

