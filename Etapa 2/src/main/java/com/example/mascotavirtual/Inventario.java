package com.example.mascotavirtual;
import java.util.ArrayList;

public class Inventario {
    private ArrayList<Item> inventario;

    public Inventario(){
        inventario = new ArrayList<Item>();
    }

    //Agrega el item al inventario
    public void agregar_items(Item item){
        inventario.add(item);
    }

    //Saca un item del inventario
    public Item sacar_item(Integer id){
        Item item_sacado = null;

        for (Item item : inventario){
            if (item.get_id() == id){
                item_sacado = item;
                item.reducir_cantidad();
                break;
            }
        }
        if (item_sacado == null)
            return null;
        if (item_sacado.get_cantidad() != null && item_sacado.get_cantidad().getValue() <= 0)
            inventario.remove(item_sacado);
        return item_sacado;
    }

}
