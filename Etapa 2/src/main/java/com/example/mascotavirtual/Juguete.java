package com.example.mascotavirtual;

public class Juguete extends Item{
    
    public Juguete(Integer id, String nombre){
        super(id, nombre, -1);
        this.felicidad = 30;
    }
}